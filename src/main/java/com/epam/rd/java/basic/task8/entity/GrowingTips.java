package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    private int tempreture;
    private String lighing;
    private int measure;
    private String attrTemp;
    private String attrWater;

    public GrowingTips(int tempreture, String lighing, int measure) {
        this.tempreture = tempreture;
        this.lighing = lighing;
        this.measure = measure;
    }

    public GrowingTips(){

    }

    public String getAttrTemp() {
        return attrTemp;
    }

    public void setAttrTemp(String attrTemp) {
        this.attrTemp = attrTemp;
    }


    public String getAttrWater() {
        return attrWater;
    }

    public void setAttrWater(String attrWater) {
        this.attrWater = attrWater;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighing() {
        return lighing;
    }

    public void setLighing(String lighing) {
        this.lighing = lighing;
    }


    public int getMeasure() {
        return measure;
    }

    public void setMeasure(int measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighing='" + lighing + '\'' +
                ", measure=" + measure +
                ", attrTemp='" + attrTemp + '\'' +
                ", attrWater='" + attrWater + '\'' +
                '}';
    }
}
