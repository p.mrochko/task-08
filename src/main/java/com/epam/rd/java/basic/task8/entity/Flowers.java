package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private List<Flower> flower;


    public List<Flower> getFlower(){
        if(flower == null)
            flower = new ArrayList<>();
        return flower;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "flower=" + flower +
                '}';
    }
}
