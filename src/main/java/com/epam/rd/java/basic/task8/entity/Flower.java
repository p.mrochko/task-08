package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private List<VisualParameters> vp;
    private List<GrowingTips> gt;

    public Flower(String name, String soil, String origin, String multiplying, List<VisualParameters> vp, List<GrowingTips> gt) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.multiplying = multiplying;
        this.vp = vp;
        this.gt = gt;
    }

    public Flower(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public List<VisualParameters> getVp() {
        if(vp == null)
            vp = new ArrayList<>();
        return this.vp;
    }

    public void setVp(List<VisualParameters> vp) {
        this.vp = vp;
    }

    public List<GrowingTips> getGt() {
            if(gt == null)
                gt = new ArrayList<>();
        return this.gt;
    }

    public void setGt(List<GrowingTips> gt) {
        this.gt = gt;
    }


    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", vp=" + vp +
                ", gt=" + gt +
                '}';
    }
}
