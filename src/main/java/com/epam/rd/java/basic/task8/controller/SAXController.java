package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private String str;
	private Flowers flowers;
	private Flower flower;
	private VisualParameters vp;
	private GrowingTips gt;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public Flowers getFlowers(){
		return flowers;
	}

	public void parse(boolean validate) throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		System.out.println("SAXParserFactory ==> " + spf.getClass());

		spf.setNamespaceAware(true);
		if(validate){
			spf.setFeature("http://xml.org/sax/features/validation" , true);
			spf.setFeature("http://apache.org/xml/features/validation/schema", true);
		}

		SAXParser parser = spf.newSAXParser();
		parser.parse(xmlFileName, this);
	}



	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		System.out.println("Start element : " + qName);
		str = localName;

		if(str.equals("flowers")){
			flowers = new Flowers();
			return;
		}

		if(str.equals("flower")){
			flower = new Flower();
			return;
		}

		if (str.equals("visualParameters")){
			vp = new VisualParameters();
			if(attributes.getLength() > 0)
				vp.setAveLen(Integer.parseInt(attributes.getValue(uri, "aveLenFlower")));
			return;
		}

		if(str.equals("lighting")){
			gt.setLighing("yes");
			return;
		}

		if(str.equals("growingTips")){
			gt = new GrowingTips();
			if(attributes.getLength() > 0){
				gt.setAttrTemp((attributes.getValue(uri, "tempreture")));
			}
			if(attributes.getLength() > 0){
				gt.setLighing("yesfesffsefsefsefse");
			}
			if(attributes.getLength() > 0){
				gt.setAttrWater((attributes.getValue(uri, "watering")));
			}
			return;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String text = new String(ch, start, length).trim();

		if(text.isEmpty())
			return;

		if(str.equals("name")){
			flower.setName(text);
			return;
		}

		if(str.equals("soil")){
			flower.setSoil(text);
			return;
		}

		if(str.equals("origin")){
			flower.setOrigin(text);
			return;
		}

		if(str.equals("stemColour")){
			vp.setStemColour(text);
		}

		if(str.equals("leafColour")){
			vp.setLeafColour(text);
			return;
		}

		if(str.equals("aveLenFlower")){
			vp.setAveLen(Integer.parseInt(text));
			return;
		}

		if(str.equals("tempreture")){
			gt.setTempreture(Integer.parseInt(text));
			return;
		}



		if(str.equals("watering")){
			gt.setMeasure(Integer.parseInt(text));
			return;
		}

		if(str.equals("multiplying")){
			flower.setMultiplying(text);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(localName.equals("flower")){
			flowers.getFlower().add(flower);
			return;
		}

		if(localName.equals("visualParameters")){
			flower.setVp(Collections.singletonList(vp));
			return;
		}

		if(localName.equals("growingTips")){
			flower.setGt(Collections.singletonList(gt));
			return;
		}
	}

	public static Comparator<Flower> compByTemp = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getGt().get(0).getTempreture() - o2.getGt().get(0).getTempreture();
		}
	};

	public static final void sortByTemp(Flowers flowers) {
		Collections.sort(flowers.getFlower(), compByTemp);
	}



}